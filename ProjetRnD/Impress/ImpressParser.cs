﻿using ProjetRnD.Slides;
using System;
using System.Xml;


namespace ProjetRnD.Impress
{
    class ImpressParser
    {
        private String _path;
        public ImpressParser(ref String t_path)
        {
            this._path = t_path;
            ExtractData("./content.xml");
        }


        private void ExtractData(string t_filePath)
        {
            XmlReader reader = XmlReader.Create(t_filePath);
            SlideShow slideShow = new SlideShow();

            Slide slide = null;
            SlideComponent slideComponent = null;

            while (reader.Read())
            {

                switch (reader.Name)
                {
                    case "style:style":
                        slideShow.styles.Add(ReadStyle(reader));
                        break;

                    case "draw:page":

                        //int pageDepth = reader.Depth;
                        if (reader.HasAttributes)
                            slide = new Slide(reader.GetAttribute("draw:name"));
                        break;

                    case "/draw:page":
                        //keep? or if slide != null add at the beggining of the draw:page ? 
                        slideShow.AddSlide(slide);
                        slide = null;
                        break;

                    case "draw:frame":
                        slideComponent = getFrame(reader);
                        break;

                    case "/draw:frame":
                        slideComponent = null;
                        break;

                    case "text:p":
                        slide.addSlideComponents(new Text(reader.ReadInnerXml(), slideComponent)); //keep the text data or loose it when beeing store as a componant ? 
                        break;

                    case "draw:image":
                        slide.addSlideComponents(new Picture(reader.GetAttribute("xlink:href"), slideComponent));
                        break;

                    case "style:font-face":
                        FontStyle fontStyle = new FontStyle(reader.GetAttribute("style:name"));
                        fontStyle.fontPitch = reader.GetAttribute("style:font-pitch");
                        fontStyle.fontFamilyGeneric = reader.GetAttribute("style:font-family-generic");
                        fontStyle.fontFamily = reader.GetAttribute("svg:font-family");
                        slideShow.styles.Add(fontStyle);
                        break;

                    case "text:list-style":
                        slideShow.styles.Add(ReadLevel(reader));
                        break;
                }
            }

            reader.Close();
        }
        //TODO: Test if the XmlReader keep the reading state when passing by reference to a function 
        private SlideComponent getFrame(in XmlReader t_reader)
        {
            SlideComponent slideComponent = new SlideComponent();

            if (!t_reader.HasAttributes)
                return null; //TODO: Deal with the exception. What to do ?

            //removing the 'cm' at the end and change the . sperator to a , so it can be changed to a double. Making it a method that get a String and return a double ? 
            slideComponent.width = double.Parse(t_reader.GetAttribute("svg:width")[..^2].Replace('.', ','));
            slideComponent.height = double.Parse(t_reader.GetAttribute("svg:height")[..^2].Replace('.', ','));
            slideComponent.x = double.Parse(t_reader.GetAttribute("svg:x")[..^2].Replace('.', ','));
            slideComponent.y = double.Parse(t_reader.GetAttribute("svg:y")[..^2].Replace('.', ','));

            slideComponent.setPresentationClass(t_reader.GetAttribute("presentation:class"));

            return slideComponent;
        }

        private Style ReadStyle(in XmlReader t_reader)
        {
            if (!t_reader.HasAttributes)
                return null;

            switch (t_reader.GetAttribute("style:family"))
            {
                //sub method ?
                case "drawing-page":
                    DrawingPageStyle dstyle = new DrawingPageStyle(t_reader.GetAttribute("style:name"));
                    while(t_reader.Name != "</style:style>")
                    {
                        t_reader.Read();
                        if (t_reader.Name == "style:drawing-page-properties")
                        {
                            dstyle.backgroundVisible = bool.Parse(t_reader.GetAttribute("presentation:background-visible"));
                            dstyle.backgroundObjectVisible = bool.Parse(t_reader.GetAttribute("presentation:background-objects-visible"));
                            dstyle.displayFooter = bool.Parse(t_reader.GetAttribute("presentation:display-footer"));
                            dstyle.displayPageNumber = bool.Parse(t_reader.GetAttribute("presentation:display-page-number"));
                            dstyle.displayDateTime = bool.Parse(t_reader.GetAttribute("presentation:display-date-time"));
                        }
                    }
                    
                    return dstyle;;

                case "graphic":

                    GraphicStyle gstyle = new GraphicStyle(t_reader.GetAttribute("style:name"));
                    while (t_reader.Name != "</style:style>")
                    {
                        t_reader.Read();
                        if (t_reader.Name == "style:graphic-properties")
                        {
                            gstyle.mirror = t_reader.GetAttribute("style:mirror");
                            //stored as a byte value (0 to 255 value for RGB color) but might not be needed. need to see how EAST use it.
                            gstyle.opacity = percentTovalue(float.Parse(t_reader.GetAttribute("draw:image-opacity")[..^1]));

                            gstyle.blue = percentTovalue(float.Parse(t_reader.GetAttribute("draw:blue")[..^1]));
                            gstyle.green = percentTovalue(float.Parse(t_reader.GetAttribute("draw:green")[..^1]));
                            gstyle.red = percentTovalue(float.Parse(t_reader.GetAttribute("draw:red")[..^1]));
                            gstyle.gamma = percentTovalue(float.Parse(t_reader.GetAttribute("draw:gamma")[..^1]));

                            gstyle.contrast = percentTovalue(float.Parse(t_reader.GetAttribute("draw:contrast")[..^1]));
                            gstyle.luminance = percentTovalue(float.Parse(t_reader.GetAttribute("draw:luminance")[..^1]));

                            gstyle.colorMode = t_reader.GetAttribute("draw:color-mode");
                            gstyle.verticalAlign = t_reader.GetAttribute("draw:textarea-vertical-align");
                            gstyle.horizontalAlign = t_reader.GetAttribute("draw:textarea-horizontal-align");
                        }
                    }

                    return gstyle;

                case "presentation":
                    PresentationStyle prstyle = new PresentationStyle(t_reader.GetAttribute("style:name"));
                    while (t_reader.Name != "</style:style>")
                    {
                        t_reader.Read();
                        if (t_reader.Name == "style:graphic-properties")
                        {
                            prstyle.minHeight = double.Parse(t_reader.GetAttribute("fo:min-height")[..^2].Replace('.', ','));
                            prstyle.AddHexColor(t_reader.GetAttribute("draw:fill-color")[^6..]);
                        }
                        else if (t_reader.Name == "style:paragraph-propertie")
                        {
                            prstyle.writingMode = t_reader.GetAttribute("style:writing-mode");
                        }
                    }

                    return prstyle;

                case "paragraph":
                    ParagraphStyle pastyle = new ParagraphStyle(t_reader.GetAttribute("style:name"));
                    while (t_reader.Name != "</style:style>")
                    {
                        t_reader.Read();
                        if (t_reader.Name == "style:paragraph-properties")
                        {
                            pastyle.writingMode = t_reader.GetAttribute("style:writing-mode");
                        }
                        else if (t_reader.Name == "loext:graphic-properties")
                        {
                            pastyle.AddHexColor(t_reader.GetAttribute("draw:fill-color")[^6..]);
                            pastyle.textAlign = t_reader.GetAttribute("fo:text-align");
                        }
                    }

                    return pastyle;

                default:
                    return null;
            }
        }

        private ListStyle ReadLevel(in XmlReader t_reader)
        {
            ListStyle listStyle = new ListStyle(t_reader.GetAttribute("style:name"));

            while (t_reader.Name != "</text:list-level-style-bullet>")
            {
                
                Level level = new Level();

                while (t_reader.Name != "</text:list-level-style-bullet>")
                {
                    t_reader.Read();

                    if(t_reader.Name == "text:list-level-style-bullet")
                    {
                        level.level = Byte.Parse(t_reader.GetAttribute("text:level"));
                        level.bulletChar = t_reader.GetAttribute("text:bullet-char");
                    }

                    if(t_reader.Name == "style:list-level-properties")
                        level.minLabelWidth = double.Parse(t_reader.GetAttribute("text:min-label-width")[..^2].Replace('.', ','));

                    if (t_reader.Name == "style:text-properties")
                    {
                        level.fontFamily = t_reader.GetAttribute("fo:font-family");
                        level.fontSize = t_reader.GetAttribute("fo:font-size");
                        level.useWindowFontColor = bool.Parse(t_reader.GetAttribute("style:use-window-font-color"));
                    }

                    listStyle.listLevel.Add(level);
                }
            }

            return listStyle;
        }

        //convert the percent value into a byte that can translate it into a percent of RBG value.
        private byte percentTovalue(float t_percent)
        {
            t_percent = t_percent / 100;
            return Convert.ToByte(Math.Round(255 * t_percent));
        }

    }
}
