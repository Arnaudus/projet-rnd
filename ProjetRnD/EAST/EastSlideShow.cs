﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using ProjetRnD.Slides;

namespace ProjetRnD.EAST
{
    class EastSlideShow
    {
        private String _name;
        public String name
        {
            get { return _name; }
            set { _name = value; }
        }

        private String _path;


        public EastSlideShow(ref String t_name, in SlideShow t_slideShow)
        {
            _name = t_name;
            createEastSlideShow(t_slideShow);
        }

        //change the litteral value by the varaible from the slideShow
        public void createEastSlideShow(in SlideShow t_slideShow)
        {
            using(XmlWriter writer = XmlWriter.Create(_path + "/" + _name))
            {
                //preferences
                writer.WriteStartElement("PREFERENCES");

                writer.WriteStartElement("PAGE");
                writer.WriteAttributeString("backcolor", "silver");
                writer.WriteAttributeString("textcolor", "black");
                writer.WriteEndElement();

                writer.WriteStartElement("TITLES");
                writer.WriteAttributeString("backcolor", "black");
                writer.WriteAttributeString("textcolor", "yellow");
                writer.WriteEndElement();

                writer.WriteStartElement("ENV");
                writer.WriteAttributeString("backcolor", "gold");
                writer.WriteAttributeString("textcolor", "pink");
                writer.WriteEndElement();

                writer.WriteEndElement();

                writer.WriteElementString("PIEDPAGE_GAUCHE", "pied gauche");
                writer.WriteElementString("PIEDPAGE_DROIT", "pied droit");

                //slower than a for loop but will be easier to write and read
                foreach(Slide slide in t_slideShow.slides)
                {

                }

            }
        }
    }
}
