﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetRnD
{
    class Text : SlideComponent
    {
        private String _text;
        public String text
        {
            get { return _text;}
            set { _text = value; }
        }

        private String _font;
        public String font
        {
            get { return _font; }
            set { _font = value; }
        }

        public Text(SlideComponent componant)
        {
            width = componant.width;
            height = componant.height; 
            x = componant.x;
            y = componant.y;
        }
    }
}
