﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetRnD.Slides
{
    class Image
    {
        private String _imagePath;
        public String imagePath
        {
            get { return _imagePath; }
            set { _imagePath = value; } // keep the set ? constror init or not ? 
        }

        private float _width;
        public float width
        {
            get { return _width; }
            set { _width = value; }
        }

        private float _height;
        public float height
        {
            get { return _height; }
            set { _height = value; }
        }
    }
}
