﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetRnD
{
    class SlideComponent
    {
        private double _x;
        public double x
        {
            get { return _x; }
            set { _x = value; }
        }

        private double _y;
        public double y
        {
            get { return _y; }
            set { _y = value; }
        }

        private double _width;
        public double width
        {
            get { return _width; }
            set { _width = value; }
        }

        private double _height;
        public double height
        {
            get { return _height; }
            set { _height = value; }
        }
    }
}
