﻿using System;
using System.IO;
using System.IO.Compression;

namespace ProjetRnD
{
    class ProjetRnD
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("ERROR: No Starting Parameters !");
                return;
            }

            
            String path = @args[0];
            String objectivePath = @"./";


            if (args.Length >= 2)
            {
                objectivePath = @args[1];
            }

            if (!File.Exists(path))
            {
                Console.WriteLine("ERROR: No Such File Or Directory !");
                return;
            }

            if (!(Path.GetExtension(path) == ".odp"))
            {
                Console.WriteLine("ERROR: File Format Not Supported !");
                return;
            }
            File.Copy(path, "ZipFile.zip");
            path = "./ZipFile.zip";

            //Path.ChangeExtension(path, ".zip");
            //path = Path.GetFileNameWithoutExtension(path);
            //path += ".zip";

            //We will work on the unzip fill to make our work and destroy it when its finished to clean it all 
            Directory.CreateDirectory("./Unzipped");
            ZipFile.ExtractToDirectory(path, "./Unzipped");
            Directory.SetCurrentDirectory("./Unzipped");


            ImpressParser parser = new ImpressParser("./");

            //String[] slides = Directory.GetFiles("./pptx/slides/");

            //for (int i = 0; i < slides.Length; i++)
            //{
            //    Console.WriteLine(slides[i]);
            //}

            Directory.SetCurrentDirectory("../");
            File.Delete("./ZipFile.zip");
            Directory.Delete("./Unzipped", true);
        }
    }
}
