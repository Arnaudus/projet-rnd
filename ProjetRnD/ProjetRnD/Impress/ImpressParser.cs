﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;


namespace ProjetRnD
{
    class ImpressParser
    {
        private String path;
        public ImpressParser(String Path)
        {
            this.path = Path;
            Openfile("./content.xml");
        }

        private void Openfile(string filePath)
        {
            XmlReader reader = XmlReader.Create(filePath);

            Slide slide = null;
            SlideComponent slideComponent = null;
            Text text = null;

            while (reader.Read())
            {

                switch (reader.Name)
                {
                    case "draw:page":

                        int pageDepth = reader.Depth;
                        if (reader.HasAttributes)
                        {
                            slide = new Slide(reader.GetAttribute("draw:name"));
                        }
                        break;

                    case "/draw:page":
                        slide = null;
                        break;

                    case "draw:frame":
                        slideComponent = new SlideComponent();
                        if (reader.HasAttributes)
                        {
                            //removing the 'cm' at the end and change the . sperator to a , so it can be changed to a double 
                            slideComponent.width = double.Parse(reader.GetAttribute("svg:width")[..^2].Replace('.',','));
                            slideComponent.height = double.Parse(reader.GetAttribute("svg:height")[..^2].Replace('.', ','));
                            slideComponent.x = double.Parse(reader.GetAttribute("svg:x")[..^2].Replace('.', ','));
                            slideComponent.y = double.Parse(reader.GetAttribute("svg:y")[..^2].Replace('.', ','));
                        }
                        break;

                    case "/draw:frame":
                        slideComponent = null;
                        break;

                    case "text:p":
                        text = new Text(slideComponent);
                        text.text = reader.ReadInnerXml();
                        Console.WriteLine(text.text);
                        break;
                }



                //Slide currentSlide = null;
                //if ((reader.NodeType == XmlNodeType.Element) && reader.Name == "draw:page")
                //{
                //    int pageDepth = reader.Depth;
                //    if (reader.HasAttributes)
                //    {
                //        currentSlide = new Slide(reader.GetAttribute("draw:name"));
                //    }
                //}
            }

            reader.Close();
        }
    }
}
