﻿using System;
using System.IO;
using System.IO.Compression;
using ProjetRnD.PowerPoint;
using ProjetRnD.Impress;

namespace ProjetRnD
{
    class ProjetRnD
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("ERROR: No Starting Parameters !");
                return;
            }

            String path = @args[0];
            String objectivePath = @"./";

            if (args.Length >= 2)
            {
                objectivePath = @args[1];
            }

            if (!File.Exists(path))
            {
                Console.WriteLine(path);
                Console.WriteLine(Directory.GetCurrentDirectory());
                Console.WriteLine("ERROR: No Such File Or Directory !");
                return;
            }

            switch (Path.GetExtension(path))
            {
                case ".pptx":
                    PowerPointParser powerPointParser = new PowerPointParser();
                    break;

                case ".odp":
                    ImpressParser impressParser = new ImpressParser(path);
                    break;

                default:
                    Console.WriteLine("ERROR: File Format Not Supported !");
                    return;
            }


            //DO it here or in the parserclass ? 
            Path.ChangeExtension(path, ".zip");

            //We will work on the unzip fill to make our work and destroy it when its finished to clean it all 
            Directory.CreateDirectory("./Unzipped");
            Directory.SetCurrentDirectory("./Unzipped");
            ZipFile.ExtractToDirectory(path, "./");

          
        }
    }
}
