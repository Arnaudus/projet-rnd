using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetRnD.Slides
{
    class Style
    {
        private String _name;
        public String name
        {
            get { return _name; }
            set { _name = value; }
        }

        //Make a bit shiffting to store the RGBA into a simple int ? 
        //using a Color Obect ? 
        // in % or in RGB values  ? -> RGB 
        private byte _red;
        public byte red
        {
            get { return _red; }
            set { _red = value; }
        }

        private byte _green;
        public byte green
        {
            get { return _green; }  
            set { _green = value; }
        }

        private byte _blue;
        public byte blue
        {
            get { return _blue; }
            set { _blue = value; }
        }


        public Style(String t_name)
        {
            _name = t_name;
        }

        //don't support the Alpha since the impress hex don't incorporate it 
        public void AddHexColor(String t_hexColor)
        {
            if (t_hexColor.Length != 6)
                return;

            _red = Byte.Parse(t_hexColor.Substring(0, 1), System.Globalization.NumberStyles.HexNumber);
            _green = Byte.Parse(t_hexColor.Substring(2, 3), System.Globalization.NumberStyles.HexNumber);
            _blue = Byte.Parse(t_hexColor.Substring(4, 5), System.Globalization.NumberStyles.HexNumber);
        }

    }
}
