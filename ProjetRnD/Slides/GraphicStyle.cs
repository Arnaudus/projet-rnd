using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetRnD.Slides
{
    class GraphicStyle : Style
    {
        //evolution to an enum ? 
        private String _colorMode;
        public String colorMode
        {
            get { return _colorMode; }
            set { _colorMode = value; }
        }

        //enum ? 
        private String _mirror;
        public String mirror
        {
            get { return _mirror; }
            set { _mirror = value; }
        }

        //enum ?
        private String _horizontalAlign;
        public String horizontalAlign
        {
            get { return _horizontalAlign; }
            set { _horizontalAlign = value; }
        }

        //same enum as horizontal ? 
        private String _verticalAlign;
        public String verticalAlign
        {
            get { return _verticalAlign; }
            set { _verticalAlign = value; }
        }

        private byte _luminance;
        public byte luminance
        {
            get { return _luminance; }
            set { _luminance = value; }
        }
        
        private byte _contrast;
        public byte contrast
        {
            get { return _contrast; }
            set { _contrast = value; }
        }

        private byte _gamma;
        public byte gamma { 
            get { return _gamma; }
            set { _gamma = value; }
        }

        private byte _opacity;
        public byte opacity
        {
            get { return _opacity; }
            set { _opacity = value; }
        }

        public GraphicStyle(String t_name) : base(t_name) { }

        //grid : internal struct / Vec4 class ? 

    }
}
