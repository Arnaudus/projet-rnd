﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetRnD.Slides
{
    class Picture : SlideComponent
    {
        private String _picturePath;
        public String picturePath
        {
            get { return _picturePath; }
            //set { _imagePath = value; } // keep the set ? constror init or not ? 
        }

        public Picture(string path, SlideComponent component)
        {
            _picturePath = path;
            width = component.width;
            height = component.height;
            x = component.x;
            y = component.y;
        }
    }
}
