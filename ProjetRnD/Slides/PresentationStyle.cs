using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetRnD.Slides
{
    class PresentationStyle : Style
    {
        private double _minHeight;
        public double minHeight
        {
            get { return _minHeight; }
            set { _minHeight = value; }
        }

        //enum ? 
        private String _writtingMode;
        public String writingMode
        {
            get { return _writtingMode; }
            set { _writtingMode = value; }
        }

        public PresentationStyle(String t_name) : base(t_name) { }

    }
}

