﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetRnD.Slides
{
    class Slide
    {
        private List<SlideComponent> _slideComponents;
        public List<SlideComponent> slideComponents
        {
            get { return _slideComponents; }
            set { _slideComponents = slideComponents; }
        }

        private string _slideName;
        public string slideName
        {
            get { return _slideName;}
        }

        public Slide(string name)
        {
            _slideName = name;
        }

        public void addSlideComponents(SlideComponent t_component) 
        {
            _slideComponents.Add(t_component);
        }
    }
}

