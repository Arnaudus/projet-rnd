﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetRnD.Slides
{
    class Text : SlideComponent
    {
        private String _text;
        public String text
        {
            get { return _text;}
            set { _text = value; }
        }

        private String _font;
        public String font
        {
            get { return _font; }
            set { _font = value; }
        }

        public Text(string text, SlideComponent component)
        {
            _text = text;
            width = component.width;
            height = component.height; 
            x = component.x;
            y = component.y;
        }
    }
}
