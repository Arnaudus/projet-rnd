﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetRnD.Slides
{
    class SlideComponent
    {
        private double _x;
        public double x
        {
            get { return _x; }
            set { _x = value; }
        }

        private double _y;
        public double y
        {
            get { return _y; }
            set { _y = value; }
        }

        private double _width;
        public double width
        {
            get { return _width; }
            set { _width = value; }
        }

        private double _height;
        public double height
        {
            get { return _height; }
            set { _height = value; }
        }

        public enum PresentationClass { Title, SubTitle, Page, Notes, Outline }

        private PresentationClass _presentationClass;
        public PresentationClass presentationClass { get;}

        public void setPresentationClass(String pclass)
        {
            switch (pclass)
            {
                case "title":
                    _presentationClass = PresentationClass.Title;
                    break;

                case "subtitle":
                    _presentationClass = PresentationClass.SubTitle;
                    break;

                case "page":
                    _presentationClass = PresentationClass.Page;
                    break;

                case "notes":
                    _presentationClass = PresentationClass.Notes;
                    break;

                case "outline":
                    _presentationClass = PresentationClass.Outline;
                    break;

            }
        }



       
    }
}
