﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetRnD.Slides
{
    class SlideShow
    {
        private List<Slide> _slides;
        public List<Slide> slides
        {
            get { return slides; }
            set { _slides = value; }
        }

        private List<Style> _styles;
        public List<Style> styles
        {
            get { return styles; }
        }

        private String _title ;
        public String title
        {
            get { return title; }
            set { _title = value; }
        }

        public void AddSlide(Slide t_slide)
        {
            _slides.Add(t_slide);
        }
    }
}
