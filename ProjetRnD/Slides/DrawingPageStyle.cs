using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetRnD.Slides
{
    class DrawingPageStyle : Style
    {
        private bool _backgroundVisible;
        public bool backgroundVisible
        {
            get { return _backgroundVisible; }
            set { _backgroundVisible = value; }
        }

        private bool _backgroundObjectVisible;
        public bool backgroundObjectVisible
        {
            get { return _backgroundObjectVisible; }
            set { _backgroundObjectVisible = value; }
        }

        private bool _displayFooter;
        public bool displayFooter
        {
            get { return _displayFooter; }
            set { _displayFooter = value; }
        }

        private bool _displayPageNumber;
        public bool displayPageNumber
        {
            get { return _displayPageNumber; }
            set { _displayPageNumber = value; }
        }

        private bool _displayDateTime;
        public bool displayDateTime
        {
            get { return _displayDateTime; }
            set { _displayDateTime = value; }
        }

        public DrawingPageStyle(String t_name) : base(t_name){ }
    }
}
