﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetRnD.Slides
{
    class FontStyle : Style
    {

        private string _fontPitch;
        public string fontPitch
        {
            get { return _fontPitch; }
            set { _fontPitch = value; }
        }

        private string _fontFamilyGeneric;
        public string fontFamilyGeneric
        {
            get { return _fontFamilyGeneric; }
            set { _fontFamilyGeneric = value; }
        }

        private string _fontFamily;
        public string fontFamily
        {
            get { return _fontFamily; }
            set { _fontFamily = value; }
        }

        public FontStyle(string t_name) : base(t_name) { }
    }
}
