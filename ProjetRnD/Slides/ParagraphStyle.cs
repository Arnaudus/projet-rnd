using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetRnD.Slides
{
    class ParagraphStyle : Style
    {
        //enum ? 
        private String _textAlign;
        public String textAlign
        {
            get { return _textAlign; }
            set { _textAlign = value; }
        }
        //enum ? 
        private String _writtingMode;
        public String writingMode
        {
            get { return _writtingMode; }
            set { _writtingMode = value; }
        }

        public ParagraphStyle(String t_name) : base(t_name) { }
    }
}
