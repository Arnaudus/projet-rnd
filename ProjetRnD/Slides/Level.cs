﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetRnD.Slides
{
    class Level
    {
        private double _minLabelwidth;
        public double minLabelWidth
        {
            get { return _minLabelwidth; }
            set { _minLabelwidth = value; }
        }

        private double _spaceBefore;
        public double spaceBefore
        {
            get { return _spaceBefore; }
            set { _spaceBefore = value; }
        }

        private string _fontFamily;
        public string fontFamily
        {
            get { return _fontFamily; }
            set { _fontFamily = value; }
        }

        private string _fontSize;
        public string fontSize
        {
            get { return _fontSize; }
            set { _fontSize = value; }
        }
        private bool _useWindowFontColor;
        public bool useWindowFontColor
        {
            get { return _useWindowFontColor; }
            set { _useWindowFontColor = value; }
        }

        private string _bulletChar;
        public string bulletChar
        {
            get { return _bulletChar; }
            set { _bulletChar = value; }
        }

        private byte _level;
        public byte level
        {
            get { return _level; }
            set { _level = value; }
        }
    }
}
