﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetRnD.Slides
{
    class ListStyle : Style
    {
        private List<Level> _listLevel;
        public List<Level> listLevel
        {
            get { return _listLevel; }
        }

        public ListStyle(string t_name) : base(t_name) { }
    }
}
