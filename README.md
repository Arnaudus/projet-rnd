# Projet RnD - WYSIWYG to WYSIWYM

## Name
WYSIWYG to WYSIWYM converter.

## Description
The software will convert LibreOffice Impress .odp presentation (WYSIWYG - What You See Is What You Get) to EAST presentation (WYSIWYM - What You See Is What You Mean).
Some data might be lost during the convertion. Please, check the resulting file is meeting your needs before using it.
Depending on the capability, the team might add other format to the converting tool (adding PowerPoints .pptx to possible source file).

## Usage
detail command line (tbd):

fileToConvert: filepath + filename to be converted (mandatory)

resultingFile: filepath + filename of the result (optionnal).
If not provided, the default filepath and filename will be the same as the file to convert with "Converted" added at the end of the file name.

## Authors and acknowledgment
It was created by Kilian MOCRET and Mélanie BAPTISTIA as an assignment for the research class in Evry university.

## License
This software is under the CC BY-NC-SA licence.

## Project status
Ongoing.

